<?php
/*
	Q2A Filter template: Template for Q2A filter modules
	Copyright (C) 2021 Jair López  https://www.question2answer.org/qa/user/jair

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
	Description: Filter Template.
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}


class q2a_filter_template
{
	// Mark 1

	// Mark 2

	// Mark 3

	// Mark 4

	// Mark 5

	// Mark 6

	// Mark 7
}
