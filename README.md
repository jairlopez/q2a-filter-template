# Q2A Filter template


Description
=================================================

*Q2A Filter template* is a Question2Answer plugin that has the basic structure
for installing a filter module.

It is meant to be an easy-to-install and easy-to-customize plugin that other
Question2Answer users can take advantage of for implementing or explaining
simple plugins.


Installation
-------------------------------------------------

* Unpack the archive into the directory `qa-plugin`


License
-------------------------------------------------
This plugin is under GNU GPLv3. See LICENSE for details.
